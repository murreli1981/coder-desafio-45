# coder-desafio-45

Framworks - Nest + Swagger

## Estructura del proyecto:

```
.
├── README.md
├── nest-cli.json
├── package-lock.json
├── package.json
├── resources
│   └── coder-house-backend.postman_collection.json
├── src
│   ├── app.controller.ts
│   ├── app.module.ts
│   ├── app.service.ts
│   ├── db
│   │   └── db.json
│   ├── dto
│   │   └── employee.dto.ts
│   ├── employees
│   │   ├── employees.controller.ts
│   │   ├── employees.module.ts
│   │   └── employees.service.ts
│   ├── interface
│   │   └── employee.interface.ts
│   └── main.ts
├── tsconfig.build.json
└── tsconfig.json
```

## Inicio del server

el server se ejecuta corriendo `npm start` que se encargará de transpilar ts y correr node

## Paths:

- `[GET] /employees` lista todos los empleados
- `[GET] /employees/:id` busca un empleado por id
- `[POST] /employees` guarda un nuevo empleado
- `[PATCH] /employees/:id` actualiza uno o varios valores de un empleado
- `[DEL] /employees/:id` elimina un empleado

### Documentación

#### Swagger

[link](http://localhost:3000/documentation/)

#### Postman

en la colección de postman: `resources/coder-house-backend.postman_collection.json`, carpeta: `coder-desafio-45` están los endpoints para probar el crud de `employees` para validarlo rapidamente hay unos empleados precargados.

🚨 Todos los endpoints están con el path `localhost:3000` 🚨
