import { ApiProperty } from '@nestjs/swagger';

export class EmployeeDto {
  @ApiProperty()
  id: string;
  @ApiProperty()
  idNumber: number;
  @ApiProperty()
  firstname: string;
  @ApiProperty()
  lastname: string;
  @ApiProperty()
  age: number;
  @ApiProperty()
  address: string;
  @ApiProperty()
  phonenumber: string;
  @ApiProperty()
  active: boolean;
}
