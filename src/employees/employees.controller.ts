import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiBody, ApiParam } from '@nestjs/swagger';
import { EmployeeDto } from 'src/dto/employee.dto';
import Employee from 'src/interface/employee.interface';
import { EmployeesService } from './employees.service';

@Controller('employees')
export class EmployeesController {
  constructor(private readonly service: EmployeesService) {}
  @Get()
  getAll(): Employee[] {
    return this.service.getAll();
  }
  @ApiParam({ name: 'id' })
  @Get('/:id')
  getById(@Param() params: any) {
    return this.service.getById(params.id);
  }
  @ApiBody({ type: EmployeeDto })
  @Post()
  create(@Body() employee: EmployeeDto) {
    return this.service.create(employee);
  }
  @ApiParam({ name: 'id' })
  @ApiBody({ type: EmployeeDto })
  @Patch('/:id')
  update(@Param() params: any, @Body() toUpdate: EmployeeDto) {
    return this.service.update(params.id, toUpdate);
  }
  @ApiParam({ name: 'id' })
  @Delete('/:id')
  delete(@Param() params) {
    return this.service.delete(params.id);
  }
}
