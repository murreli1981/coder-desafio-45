import { Injectable } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';
import Employee from 'src/interface/employee.interface';
import data from '../db/db.json';
@Injectable()
export class EmployeesService {
  private db: any = data;

  getAll(): Employee[] {
    return this.db;
  }

  getById(id: String): Employee {
    return this.db.find((emp) => emp.id === id);
  }

  create(employee: Employee): Employee {
    let employeeToCreate: Employee;
    employeeToCreate = employee;
    employeeToCreate.id = uuidv4();
    this.db.push(employeeToCreate);
    return employeeToCreate;
  }

  update(id: String, payload: any) {
    let employeeUpdated: Employee;
    this.db = this.db.map((emp) => {
      if (emp.id === id) {
        employeeUpdated = { ...emp, ...payload };
        return employeeUpdated;
      } else return emp;
    });
    return employeeUpdated;
  }

  delete(id: String) {
    let employeeDeleted: Employee;
    this.db = this.db.filter((emp) => {
      if (emp.id === id) {
        employeeDeleted = emp;
        return;
      } else return emp;
    });
    return employeeDeleted;
  }
}
