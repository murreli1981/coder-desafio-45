import { ApiProperty } from '@nestjs/swagger';

export default interface Employee {
  id: string;
  idNumber: number;
  firstname: string;
  lastname: string;
  age: number;
  address: string;
  phonenumber: string;
  active: boolean;
}
