import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const opts = new DocumentBuilder()
    .setTitle('Employee API')
    .setDescription('ejemplo de CRUD con NestJS')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, opts);

  SwaggerModule.setup('documentation', app, document);
  await app.listen(3000);
}
bootstrap();
